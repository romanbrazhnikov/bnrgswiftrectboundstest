//
//  ViewController.swift
//  bnrgSwiftRectBoundsTest
//
//  Created by Roman Brazhnikov on 22/03/2019.
//  Copyright © 2019 Roman Brazhnikov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let firstFrame = CGRect(x: 100, y: 100, width: 50, height: 150)
        let firstView = UIView(frame: firstFrame)
        
        firstView.backgroundColor = UIColor.init(red: 0.1, green: 0.5, blue: 0.5, alpha: 0.8)
        view.addSubview(firstView)
        
        let secondFrame = CGRect(x: 30, y: 25, width: 15, height: 20)
        let secondView = UIView(frame: secondFrame)
        
        secondView.backgroundColor = UIColor.init(red: 200.0/255, green: 250.0/255, blue: 100.0/255, alpha: 0.9)
        //view.addSubview(secondView)
        
        firstView.addSubview(secondView)
    }


}

